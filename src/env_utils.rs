use std::env::var;
use std::process::exit;

use dotenv::from_filename;

pub(crate) fn get_env_or_exit(key: &str) -> String {
    match var(key) {
        Ok(var) => var,
        Err(_e) => {
            log::error!(r#"Missing env key "{}""#, key);
            exit(1);
        }
    }
}

pub(crate) fn string_to_u16(val: String) -> u16 {
    match val.parse::<u16>() {
        Ok(o) => o,
        Err(_) => {
            log::error!(r#"Bad value "{}", required type: "u16""#, val);
            exit(1);
        }
    }
}

pub(crate) fn string_to_i64(val: String) -> i64 {
    match val.parse::<i64>() {
        Ok(o) => o,
        Err(_) => {
            log::error!(r#"Bad value "{}", required type: "i64""#, val);
            exit(1);
        }
    }
}

pub(crate) fn load_env() {
    let success_local = match from_filename(".env.local") {
        Ok(_) => true,
        Err(_e) => false,
    };
    let success = match from_filename(".env") {
        Ok(_) => true,
        Err(_e) => false,
    };

    if !success && !success_local {
        log::error!(".env/.env.local load error!");

        exit(1)
    }
}
