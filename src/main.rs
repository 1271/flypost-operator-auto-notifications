extern crate dotenv;
extern crate log;
extern crate pretty_env_logger;
extern crate redis;
extern crate regex;
extern crate reqwest;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate tokio;

use std::env;
use std::process::exit;

use redis::{ConnectionAddr, ConnectionInfo};
use tokio::stream::StreamExt;

use env_utils::{get_env_or_exit, load_env, string_to_i64, string_to_u16};

mod env_utils;

#[derive(Debug, Serialize, Clone)]
struct WsClientMessageWrapper {
    user_ids: Option<Vec<String>>,
    key: String,
    topic: String,
    message: String,
}


#[tokio::main]
async fn main() {
    load_env();
    init_logger();

    log::info!("Daemon init v. {}", env!("CARGO_PKG_VERSION"));

    log::info!("Regex: {}", get_env_or_exit("STORAGE_REGEX").as_str());

    let key_reg_template = regex::Regex::new(get_env_or_exit("STORAGE_REGEX").as_str())
        .expect("Bad storage regex!");

    let ws_url = get_env_or_exit("WS_PUSH_HOST");

    let ws_key = get_env_or_exit("PUBLISH_SECRET_KEY");

    let topic_key = get_env_or_exit("TOPIC_KEY");

    let template = get_env_or_exit("MESSAGE_TEMPLATE");

    let client = redis::Client::open(ConnectionInfo {
        addr: Box::from(ConnectionAddr::Tcp(
            get_env_or_exit("REDIS_HOST"),
            string_to_u16(get_env_or_exit("REDIS_PORT")),
        )),
        db: string_to_i64(get_env_or_exit("REDIS_DB")),
        username: None,
        passwd: env::var("REDIS_KEY").ok(),
    }).expect("Failed connection to redis");

    let mut pubsub_conn = match client.get_async_connection().await {
        Ok(c) => {
            log::debug!("Redis pubsub connected");
            c.into_pubsub()
        },
        Err(e) => {
            log::error!(r#"Redis pubsub connection error: "{:?}""#, e);
            exit(1);
        }
    };

    // по какой-то причине тут нельзя указать паттерн ключа, поэтому проверяем его ниже
    match pubsub_conn.psubscribe("*").await {
        Ok(_) => {
            log::debug!("Redis pubsub subscribed");
        },
        Err(e) => {
            log::error!(r#"Redis pubsub subscribe error: "{}""#, e);
            exit(1);
        }
    }

    let mut pubsub_stream = pubsub_conn.on_message();

    log::info!("Daemon started");

    loop {
        if let Some(message) = pubsub_stream.next().await {
            if "__keyevent@0__:expired" != message.get_channel().unwrap_or(String::new()) {
                continue;
            }

            let pubsub_msg: String = message.get_payload().unwrap_or(String::new());

            log::debug!(r#"msg: "{}""#, pubsub_msg.clone());

            if !key_reg_template.clone().is_match(pubsub_msg.clone().as_str()) {
                log::debug!("Skip wrong key {}", pubsub_msg.clone());
                continue;
            }

            let msg = pubsub_msg.clone();

            let re_message = key_reg_template.clone()
                .replace_all(msg.as_str(), template.clone().as_str());

            let msg = pubsub_msg.clone();
            let topic_ = topic_key.clone();

            let topic = key_reg_template.clone()
                .replace_all(msg.as_str(), topic_.as_str());

            log::debug!("Message: {}", re_message.clone());
            log::debug!("Topic: {}", topic.clone());

            let ws_message = WsClientMessageWrapper {
                user_ids: None,
                key: ws_key.clone(),
                topic: topic.to_string(),
                message: re_message.to_string(),
            };

            let prepared_request = reqwest::Client::new()
                .post(&ws_url.clone())
                .json(&ws_message)
                .send();

            match prepared_request.await {
                Ok(r) => {
                    log::debug!("Message success send");

                    match r.text().await {
                        Ok(body) => log::debug!("{}", body),
                        Err(_) => ()
                    }
                },
                Err(e) => {
                    log::warn!("Failed send message: {}", e.to_string());
                    continue;
                }
            }
        }
    };
}

fn init_logger() {
    let mut log_builder = pretty_env_logger::formatted_timed_builder();
    if let Ok(filter) = env::var("DAEMON_LOG") {
        log_builder.parse_filters(&filter);
    }
    log_builder.init();
}
